# Prototype d'un jeu basé sur ses dialogues

*Ceci est une réflexion et une première proposition technique tentant de rendre un jeu entier uniquement jouable par ses dialogues*

## Bases importantes

- 1/. Définir une structure de données[^1] pour représenter les dialogues :

	- Utiliser des arbres de dialogues, où chaque nœud représente un morceau de dialogue (une ligne ou une réplique) et les branches représentent les choix possibles pour le joueur. Chaque nœud peut contenir des informations telles que le texte du dialogue, les choix disponibles, les conséquences des choix, etc.

- 2/. Concevoir une logique de suivi des états :

	- Utiliser de variables d'état pour suivre l'évolution du jeu et des interactions avec les personnages non-joueurs (PNJ). Les états peuvent être des variables booléennes, des compteurs, des valeurs numériques, etc., qui décrivent l'état du monde du jeu et influencent les dialogues.

- 3/. Implémenter un moteur de dialogue :

	- Créer des fonctions ou modules gèrant le flux du dialogue. À partir de l'état actuel, déterminer quels nœuds de dialogue sont disponibles pour le joueur. Afficher le texte du dialogue et les choix possibles à l'écran. Attendre que le joueur fasse un choix et mettre à jour l'état en fonction de ce choix.

- 4/. Gérer les choix du joueur :

	- En fonction du choix du joueur, modifier l'état du jeu en conséquence. Cela peut inclure des modifications de variables d'état, l'activation de quêtes, l'ouverture de nouvelles options de dialogue, etc.

- 5/. Gérer les conséquences des choix :

	- Les choix du joueur peuvent avoir un impact sur le déroulement futur du jeu et les dialogues. Concevoir des mécanismes pour refléter les conséquences des choix du joueur. Cela peut inclure des changements dans le comportement des PNJ, des événements déclenchés, des réactions d'autres personnages, etc.

- 6/. Tester et itérer :

	- Tester le système à différentes stades de développement pour s'assurer qu'il fonctionne correctement et offre une expérience de jeu engageante. Demander des retours pour identifier les points faibles et les améliorations possibles, itérer sur l'algorithme en conséquence.

	**Plus tard...**

- 7/. Implémenter des techniques avancées :

	- Selon l'ambition du système de dialogue, intégrer des techniques plus avancées telles que l'apprentissage automatique pour la génération de dialogues, l'analyse des émotions du joueur, la génération de réponses dynamiques, etc.
	
## Proto-algorithme

Exemple d'algorithme en pseudo-code pour un système de dialogue qui conserve en mémoire les choix précédents et détermine dans le temps la personnalité du personnage incarné par joueur et adapte les dialogues futurs en fonction des choix précédents :

```
Fonction Dialogue():
    État <- ÉtatInitial  // Initialisation de l'état du jeu
    PersonnalitéJoueur <- PersonnalitéNeutre  // Initialisation de la personnalité du joueur
    
    Tant que dialogue en cours:
        NoeudDialogue <- SélectionnerNoeudDialogue(État, PersonnalitéJoueur)  // Sélectionne le prochain nœud de dialogue en fonction de l'état et de la personnalité du joueur
        
        Afficher(NoeudDialogue.Texte)  // Affiche le texte du nœud de dialogue
        
        Si NoeudDialogue.ChoixDisponibles est vide:
            FinDialogue()  // Aucun choix disponible, fin du dialogue
        Sinon:
            AfficherChoix(NoeudDialogue.ChoixDisponibles)  // Affiche les choix disponibles
            
            ChoixJoueur <- AttendreChoixJoueur()  // Attend que le joueur fasse un choix
            
            MettreÀJourÉtat(ChoixJoueur, État)  // Met à jour l'état en fonction du choix du joueur
            MettreÀJourPersonnalité(ChoixJoueur, PersonnalitéJoueur)  // Met à jour la personnalité du joueur en fonction du choix
            
    FinDialogue()

Fonction SélectionnerNoeudDialogue(État, PersonnalitéJoueur):
    // Logique pour sélectionner le prochain nœud de dialogue en fonction de l'état et de la personnalité du joueur
    // Retourne le nœud de dialogue sélectionné

Fonction MettreÀJourÉtat(ChoixJoueur, État):
    // Logique pour mettre à jour l'état du jeu en fonction du choix du joueur
    // Met à jour État en conséquence

Fonction MettreÀJourPersonnalité(ChoixJoueur, PersonnalitéJoueur):
    // Logique pour mettre à jour la personnalité du joueur en fonction du choix
    // Met à jour PersonnalitéJoueur en conséquence

Fonction FinDialogue():
    // Logique pour terminer le dialogue
```

L'algorithme ci-dessus utilise des fonctions abstraites[^2] (``SélectionnerNoeudDialogue``, ``MettreÀJourÉtat``, ``MettreÀJourPersonnalité``, et ``FinDialogue``) pour lesquelles il faudra ajouter une logique spécifique au jeu. Ces fonctions seront responsables de la sélection des nœuds de dialogue appropriés, de la mise à jour de l'état du jeu et de la personnalité du joueur, ainsi que de la gestion de la fin de chaque dialogue.

L'algorithme doit être adapté à la structure de données (à définie plus tard), ainsi qu'aux mécanismes de personnalité et d'influence des choix mis en place.

## Structure de données

Ebauche d'une structure de données, le contenu (les attributs) de chaque structure est à définir...

```
Structure DialogueNode:
    Texte: Chaîne de caractères  // Le texte du nœud de dialogue
    ChoixDisponibles: Liste de ChoixDialogue  // Les choix disponibles pour le joueur dans ce nœud

Structure ChoixDialogue:
    Texte: Chaîne de caractères  // Le texte du choix
    Action: ActionDialogue  // L'action associée au choix (mise à jour de l'état, de la personnalité, etc.)

Structure ActionDialogue:
    Type: Enumération  // Le type d'action (par exemple, MiseAJourEtat, MiseAJourPersonnalite, etc.)
    Paramètres: Dictionnaire  // Les paramètres spécifiques à l'action (par exemple, état à mettre à jour, valeur de personnalité, etc.)

Structure EtatJeu:
    # Variables d'état nécessaires au jeu (par exemple, des booléens, des compteurs, etc.)

Structure PersonnaliteJoueur:
    # Attributs de personnalité du joueur qui peuvent être influencés par les choix de dialogue
```

Cette structure de données utilise des objets[^3] pour représenter les différents éléments du dialogue. ``DialogueNode`` représente un nœud de dialogue avec son texte et les choix disponibles. Chaque choix est défini par la structure ``ChoixDialogue``, qui contient le texte du choix et l'action associée.

L'action est représentée par la structure ``ActionDialogue``, qui spécifie le type d'action à effectuer (par exemple la mise à jour de l'état ou de la personnalité) et les paramètres spécifiques à l'action.

Enfin, les structures ``EtatJeu`` et ``PersonnaliteJoueur`` pour stocker les variables d'état et les attributs de personnalité du joueur respectivement.

[^1]: Une structure de données est un format destiné à organiser, traiter, extraire et stocker des données. Par exemple : les structures de données séquentielles (tableaux), les structures de données linéaires (liste chaînées), les arbres (noeuds, feuilles...),  les graphes.

[^2]: Pour faire simple, le comportement de la fonction peut être redéfini (ce qui est utile pour notre jeu qui doit s'adapter en fonctions des choix du joueur)

[^3]: Un objet peut être vu comme une boite dans laquelle il existe un nombre défini de compartiments et où chaque compartiment peut contenir un type bien spécifique (par exemple une valeur entière, une chaîne de caractère, un autre objet...) 